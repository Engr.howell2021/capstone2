//[SECTION] Dependencies and Modules
const mongoose = require("mongoose"); 

//[SECTION] Schema / Document Blueprint
  const productsSchema = new mongoose.Schema({
    ProductName: {
        type: String,
        required: [true, "productname is required"]
    },
    description: {
        type: String,
        required: [true, "Description is required"]
    },
    price: {
        type: Number,
        required: [true, "Price is required"]
    },
    isActive:{
        type: Boolean,
        default: true
    },
    createdOn: {
        type: Date,
        default: new Date()
    }

})

//[SECTION] Model
   const Product = mongoose.model("Product", productsSchema);
   module.exports = Product; 
