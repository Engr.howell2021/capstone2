//[SECTION] Dependencies and Modules
const auth = require('../auth')
const exp = require("express");
const { send } = require("express/lib/response");
const { checkNonAdmin } = require("../auth");
const controller = require('../controllers/orders');

// destructive verify form auth
const {verify} = auth;

//[SECTION] Routing Component
const route = exp.Router(); 


// route.post('/createOrder',verify,(req,res)=>{
//     let data = req.body;
//     let user = req.user;
//     controller.orderCreate(user,data).then(result =>{
//         res.send(result);
//     })
// })


//Routes for getting all orders
route.get('/allorders', auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization)

    controller.getAllOrder(userData)
    .then(result => res.send(result))
    .catch(error => res.send(error))
})


//Routes for getting my Orders
route.get('/myOrders', auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization)

    controller.getAllMyOrder(userData)
    .then(result => res.send(result))
    .catch(error => res.send(error))
})


route.post("/createorder", auth.verify,(req, res) => {
	let orders = {
		userId: auth.decode(req.headers.authorization).id,
		productId: req.body.productId,
		quantity: req.body.quantity
	}
	controller.checkout(orders).then(result => res.send(result))
})






module.exports = route; 
